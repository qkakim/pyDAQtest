import numpy as array
import numpy as np

"""make self trig data - it need to modify"""
ch1 = np.random.randint(2, size=100)
ch2 = np.random.randint(2, size=100)
ch3 = np.random.randint(2, size=100)
ch4 = np.random.randint(2, size=100)

ch1 = np.asarray(ch1)
ch2 = np.asarray(ch2)
ch3 = np.asarray(ch3)
ch4 = np.asarray(ch4)

"""make clk"""
clk_ch1 = []
clk_ch2 = []
clk_ch3 = []
clk_ch4 = []

n1 = ch1.shape[0]
n2 = ch2.shape[0]
n3 = ch3.shape[0]
n4 = ch4.shape[0]

i1 = 0
i2 = 0
i3 = 0
i4 = 0

while n1 :
    i1 = i1 + 1
    clk_ch1.append(i1)

while n2 :
    i2 = i2 + 1
    clk_ch2.append(i2)

while n3 :
    i3 = i3 + 1
    clk_ch3.append(i3)

while n4 :
    i4 = i4 + 1
    clk_ch4.append(i4)

"""synchronize signal and clk"""
nch1 = []
nch1 = np.stack((ch1,clk), axis = -1)
nch2 = []
nch2 = np.stack((ch2,clk), axis = -1)
nch3 = []
nch3 = np.stack((ch3,clk), axis = -1)
nch4 = []
nch4 = np.stack((ch4,clk), axis = -1)

"""save channel data"""
np.savetxt('ch1.txt',nch1)
np.savetxt('ch2.txt',nch2)
np.savetxt('ch3.txt',nch3)
np.savetxt('ch4.txt',nch4)
