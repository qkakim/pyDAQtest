import numpy as array
import numpy as np

"""make self trig data - it need to modify"""
ch1 = np.random.randint(2, size=100, dtype='int')
ch2 = np.random.randint(2, size=100, dtype='int')
ch3 = np.random.randint(2, size=100, dtype='int')
ch4 = np.random.randint(2, size=100, dtype='int')

ch1 = np.asarray(ch1)
ch2 = np.asarray(ch2)
ch3 = np.asarray(ch3)
ch4 = np.asarray(ch4)


"""save channel data"""
np.savetxt('./ch/ch1.txt',ch1)
np.savetxt('./ch/ch2.txt',ch2)
np.savetxt('./ch/ch3.txt',ch3)
np.savetxt('./ch/ch4.txt',ch4)
