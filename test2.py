import os
import numpy as arrary
import numpy as np

TB1 = 0
TB2 = 0
TB3 = 0
TB4 = 0
cnt1 = cnt2 = cnt3 = cnt4 = 0
DT1 = DT2 = DT3 = DT4 = 3

"""file path"""
PREFIX = "/home/kimwootae/work/pyDAQtest/ch"

"""channel array after self trig 
   and need to synchronize signal(self trig ; true of false) and clk in each channel"""
nch1 = []
nch2 = []
nch3 = []
nch4 = []

with open(os.path.join(PREFIX, "ch1.txt"), "r") as f1 :
    nch1 = np.loadtxt(f1)
with open(os.path.join(PREFIX, "ch2.txt"), "r") as f2 :
    nch2 = np.loadtxt(f2)
with open(os.path.join(PREFIX, "ch3.txt"), "r") as f3 :
    nch3 = np.loadtxt(f3)
with open(os.path.join(PREFIX, "ch4.txt"), "r") as f4 :
    nch4 = np.loadtxt(f4)

"""to do deadtime argorizm each channel array"""

nnch1 = []
nnch2 = []
nnch3 = []
nnch4 = []
for c1 in nch1 :
    if c1 == 1 :
        if TB1 == 1 :
            cnt1 = cnt1 + 1
            if cnt1 == DT1 :
                cnt1 = 0
                TB1 = 0
            else : pass
        else :
            TB1 = 1
            cnt1 = cnt1 + 1
    else :
        if TB1 == 1 :
            cnt1 = cnt1 + 1
            if cnt1 == DT1 :
                cnt1 = 0
                TB1 = 0
            else : pass
        else :
            pass
    nnch1.append([c1, TB1, cnt1, DT1])
np.savetxt('./nch/nch1.txt',nnch1)

for c2 in nch2 :
    if c2 == 1 :
        if TB2 == 1 :
            cnt2 = cnt2 + 1
            if cnt2 == DT2 :
                cnt2 = 0
                TB2 = 0
            else : pass
        else :
            TB2 = 1
            cnt2 = cnt2 + 1
    else :
        if TB2 == 1 :
            cnt2 = cnt2 + 1
            if cnt2 == DT2 :
                cnt2 = 0
                TB2 = 0
            else : pass
        else :
            pass
    nnch2.append([c2, TB2, cnt2, DT2])
np.savetxt('./nch/nch2.txt',nnch2)

for c3 in nch3 :
    if c3 == 1 :
        if TB3 == 1 :
            cnt3 = cnt3 + 1
            if cnt3 == DT3 :
                cnt3 = 0
                TB3 = 0
            else : pass
        else :
            TB3 = 1
            cnt3 = cnt3 + 1
    else :
        if TB3 == 1 :
            cnt3 = cnt3 + 1
            if cnt3 == DT3 :
                cnt3 = 0
                TB3 = 0
            else : pass
        else :
            pass
    nnch3.append([c3, TB3, cnt3, DT3])
np.savetxt('./nch/nch3.txt',nnch3)

for c4 in nch4 :
    if c4 == 1 :
        if TB4 == 1 :
            cnt4 = cnt4 + 1
            if cnt4 == DT4 :
                cnt4 = 0
                TB4 = 0
            else : pass
        else :
            TB4 = 1
            cnt4 = cnt4 + 1
    else :
        if TB4 == 1 :
            cnt4 = cnt4 + 1
            if cnt4 == DT4 :
                cnt4 = 0
                TB4 = 0
            else : pass
        else :
            pass
    nnch4.append([c4, TB4, cnt4, DT4])
np.savetxt('./nch/nch4.txt',nnch4)
