import os
import numpy as arrary
import numpy as np
import ROOT

TB1 = 0
TB2 = 0
TB3 = 0
TB4 = 0
cnt1 = cnt2 = cnt3 = cnt4 = 0
DT1 = DT2 = DT3 = DT4 = 3

"""tree and branch"""
rf = ROOT.TFile("daq.root", "recreate")
rt1 = ROOT.TTree("ch1","tree title")
rt2 = ROOT.TTree("ch2","tree title")
rt3 = ROOT.TTree("ch3","tree title")
rt4 = ROOT.TTree("ch4","tree title")

rt1.Branch('trgBlock_ch1', TB1, 'trgBlock_ch1/D')
rt2.Branch('trgBlock_ch2', TB2, 'trgBlock_ch2/D')
rt3.Branch('trgBlock_ch3', TB3, 'trgBlock_ch3/D')
rt4.Branch('trgBlock_ch4', TB4, 'trgBlock_ch4/D')
rt1.Branch('deadtime_count_ch1', cnt1, 'deadtime_count_ch1/D')
rt2.Branch('deadtime_count_ch2', cnt2, 'deadtime_count_ch2/D')
rt3.Branch('deadtime_count_ch3', cnt3, 'deadtime_count_ch3/D')
rt4.Branch('deadtime_count_ch4', cnt4, 'deadtime_count_ch4/D')


"""file path"""
PREFIX = "/home/kimwootae/work/pyDAQtest/ch"

"""channel array after self trig 
   and need to synchronize signal(self trig ; true of false) and clk in each channel"""
nch1 = []
nch2 = []
nch3 = []
nch4 = []

with open(os.path.join(PREFIX, "ch1.txt"), "r") as f1 :
    nch1 = np.loadtxt(f1)
with open(os.path.join(PREFIX, "ch2.txt"), "r") as f2 :
    nch2 = np.loadtxt(f2)
with open(os.path.join(PREFIX, "ch3.txt"), "r") as f3 :
    nch3 = np.loadtxt(f3)
with open(os.path.join(PREFIX, "ch4.txt"), "r") as f4 :
    nch4 = np.loadtxt(f4)

"""to do deadtime argorizm each channel array"""

nnch1 = []
nnch2 = []
nnch3 = []
nnch4 = []
for c1 in nch1 :
    if c1 == 1 :
        if TB1 == 1 :
            cnt1 = cnt1 + 1
            if cnt1 == DT1 :
                cnt1 = 0
                TB1 = 0
            else : pass
        else :
            TB1 = 1
            cnt1 = cnt1 + 1
    else :
        if TB1 == 1 :
            cnt1 = cnt1 + 1
            if cnt1 == DT1 :
                cnt1 = 0
                TB1 = 0
            else : pass
        else :
            pass
    nnch1.append([c1, TB1, cnt1, DT1])
    rt1.Fill()
np.savetxt('./nch/nch1.txt',nnch1)

for c2 in nch2 :
    if c2 == 1 :
        if TB2 == 1 :
            cnt2 = cnt2 + 1
            if cnt2 == DT2 :
                cnt2 = 0
                TB2 = 0
            else : pass
        else :
            TB2 = 1
            cnt2 = cnt2 + 1
    else :
        if TB2 == 1 :
            cnt2 = cnt2 + 1
            if cnt2 == DT2 :
                cnt2 = 0
                TB2 = 0
            else : pass
        else :
            pass
    nnch2.append([c2, TB2, cnt2, DT2])
    rt2.Fill()
np.savetxt('./nch/nch2.txt',nnch2)

for c3 in nch3 :
    if c3 == 1 :
        if TB3 == 1 :
            cnt3 = cnt3 + 1
            if cnt3 == DT3 :
                cnt3 = 0
                TB3 = 0
            else : pass
        else :
            TB3 = 1
            cnt3 = cnt3 + 1
    else :
        if TB3 == 1 :
            cnt3 = cnt3 + 1
            if cnt3 == DT3 :
                cnt3 = 0
                TB3 = 0
            else : pass
        else :
            pass
    nnch3.append([c3, TB3, cnt3, DT3])
    rt3.Fill()
np.savetxt('./nch/nch3.txt',nnch3)

for c4 in nch4 :
    if c4 == 1 :
        if TB4 == 1 :
            cnt4 = cnt4 + 1
            if cnt4 == DT4 :
                cnt4 = 0
                TB4 = 0
            else : pass
        else :
            TB4 = 1
            cnt4 = cnt4 + 1
    else :
        if TB4 == 1 :
            cnt4 = cnt4 + 1
            if cnt4 == DT4 :
                cnt4 = 0
                TB4 = 0
            else : pass
        else :
            pass
    nnch4.append([c4, TB4, cnt4, DT4])
    rt4.Fill()
np.savetxt('./nch/nch4.txt',nnch4)

rf.Write()
rf.Close()
